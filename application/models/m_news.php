<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_news extends CI_Model {

	var $table = 'tbl_news';

	public function get_all() {

		$this->db->select('id_news,tbl_categories.id_categories,categories,,tbl_type.id_type,type,tbl_user.id_user,name,title,content,active,tbl_news.created_at,tbl_news.modified_at,tbl_news.deleted_at');
		$this->db->join('tbl_categories','tbl_categories.id_categories = tbl_news.id_categories','inner');
		$this->db->join('tbl_type','tbl_type.id_type = tbl_news.id_type','inner');
		$this->db->join('tbl_user','tbl_user.id_user = tbl_news.id_user','inner');
		$this->db->where('tbl_news.deleted_at', null);
		$this->db->order_by('tbl_news.created_at','DESC');
		$this->db->order_by('tbl_news.modified_at','DESC');
		$query = $this->db->get($this->table);
		return $query->result();

	}

		public function get_all_peruser($id) {

		$this->db->select('id_news,tbl_categories.id_categories,categories,,tbl_type.id_type,type,tbl_user.id_user,name,title,content,active,tbl_news.created_at,tbl_news.modified_at,tbl_news.deleted_at');
		$this->db->join('tbl_categories','tbl_categories.id_categories = tbl_news.id_categories','inner');
		$this->db->join('tbl_type','tbl_type.id_type = tbl_news.id_type','inner');
		$this->db->join('tbl_user','tbl_user.id_user = tbl_news.id_user','inner');
		$this->db->where('tbl_news.deleted_at', null);
		$this->db->where('tbl_news.id_user', $id);
		$this->db->order_by('tbl_news.created_at','DESC');
		$this->db->order_by('tbl_news.modified_at','DESC');
		$query = $this->db->get($this->table);
		return $query->result();

	}

	public function get_main() {
		$this->db->select('id_news,tbl_categories.id_categories,categories,tbl_type.id_type,type,tbl_user.id_user,name,title,content,active,tbl_news.created_at,tbl_news.modified_at,tbl_news.deleted_at');
		$this->db->join('tbl_categories','tbl_categories.id_categories = tbl_news.id_categories','inner');
		$this->db->join('tbl_type','tbl_type.id_type = tbl_news.id_type','inner');
		$this->db->join('tbl_user','tbl_user.id_user = tbl_news.id_user','inner');
		$this->db->where('tbl_news.deleted_at', null);
		$this->db->where('tbl_news.active', 'yes');
		$this->db->order_by('tbl_news.created_at','DESC');
		$this->db->order_by('tbl_news.modified_at','DESC');
		$query = $this->db->get($this->table);
		return $query->result();
	}

	public function get_id($id) {

		$this->db->select('id_news,tbl_categories.id_categories,categories,tbl_type.id_type,type,tbl_user.id_user,name,title,content,active,tbl_news.created_at,tbl_news.modified_at,tbl_news.deleted_at');
		$this->db->join('tbl_categories','tbl_categories.id_categories = tbl_news.id_categories','inner');
		$this->db->join('tbl_type','tbl_type.id_type = tbl_news.id_type','inner');
		$this->db->join('tbl_user','tbl_user.id_user = tbl_news.id_user','inner');
		$this->db->where('tbl_news.deleted_at', null);
		$this->db->where('id_news', $id);
		$query = $this->db->get($this->table);
		return $query->result();

	}

	public function post_data($data) {

		$this->db->insert($this->table, $data);
		return TRUE;

	}

	public function put_data($data,$id)  {

	   $this->db->where('id_news', $id);
	   $this->db->update($this->table,$data);
	   return TRUE;

	}

	public function softdel_data($data,$id)  {

	   $this->db->where('id_news', $id);
	   $this->db->update($this->table,$data);
	   return TRUE;

	}

	public function deactive_data($data,$id) {

		$this->db->where('id_news', $id);
		$this->db->update($this->table,$data);
		return TRUE;
	}

	public function active_data($data,$id) {

		$this->db->where('id_news', $id);
		$this->db->update($this->table,$data);
		return TRUE;
	}

	// public function delete_data($id) {

	// 	$this->db->where('id_news', $id);
	// 	$this->db->delete($this->table);
	// 	return TRUE;
	// }

}

/* End of file m_news.php */
/* Location: ./application/models/m_news.php */