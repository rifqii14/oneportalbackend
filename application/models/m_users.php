<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_users extends CI_Model {

	var $table = 'tbl_user';

	public function get_all() {

		$this->db->select('*');
		$this->db->where('deleted_at', null);
		$query = $this->db->get($this->table);
		return $query->result();

	}

	public function get_id($id) {

		$this->db->select('*');
		$this->db->where('id_user', $id);
		$query = $this->db->get($this->table);
		return $query->result();

	}

	public function post_data($data) {

		$this->db->insert($this->table, $data);
		return TRUE;

	}

	public function put_data($data,$id)  {

	   $this->db->where('id_user', $id);
	   $this->db->update($this->table,$data);
	   return TRUE;

	}

	public function softdel_data($data,$id)  {

	   $this->db->where('id_user', $id);
	   $this->db->update($this->table,$data);
	   return TRUE;

	}

	// public function delete_data($id) {

	// 	$this->db->where('id_categories', $id);
	// 	$this->db->delete($this->table);
	// 	return TRUE;
	// }
}

/* End of file m_users.php */
/* Location: ./application/models/m_users.php */