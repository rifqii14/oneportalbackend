<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_type extends CI_Model {

	var $table = 'tbl_type';

	public function get_all() {
		
		$query = $this->db->get($this->table);
		return $query->result();

	}

}

/* End of file m_type.php */
/* Location: ./application/models/m_type.php */