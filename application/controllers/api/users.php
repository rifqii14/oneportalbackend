<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Users extends REST_Controller {

	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('m_users','mu');
	}

	public function user_get() {
		$id = $this->get('id_user');

		if(!$id) {
			$this->response(NULL,400);
		}

		$users = $this->mu->get_id($id);

		if($users) {
			$this->response($users, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No users were found'
			], 404); 
		}

	}

	public function index_get() {
		$users = $this->mu->get_all();

		if($users) {
			$this->response($users, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No users were found'
			], 404); 
		}
	}

	public function index_post() {

		$data = array(
		    'id_user' => $this->post('id_user'),
		    'name' => $this->post('name'),
		    'username' => $this->post('username'),
		    'password' => md5($this->post('password')),
		    'privileges' => $this->post('privileges'),
		    'created_at' => date('Y-m-d H:i:s'),
		    'modified_at' => null,
		    'deleted_at' => null,
		);

		$res = $this->mu->post_data($data);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success, data inserted',
				'data' => $data
				],200);
		}

	}

	public function index_put() {

		$id = $this->put('id_user');
		$data = array(
			'id_user' => $this->put('id_user'),
			'name' => $this->put('name'),
			'username' => $this->put('username'),
			'password' => md5($this->put('password')),
			'privileges' => $this->put('privileges'),
			'modified_at' => date('Y-m-d H:i:s')
		);

		$res = $this->mu->put_data($data,$id);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success, data updated',
				'data' => $data
				],200);
		}

 
	}

	public function del_put() {

		$id = $this->put('id_user');
		$data = array(
			'id_user' => $this->put('id_user'),
			'deleted_at' => date('Y-m-d H:i:s')
		);

		$res = $this->mu->softdel_data($data,$id);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success, data deleted',
				'data' => $data
				],200);
		}

	}


}

/* End of file users.php */
/* Lousersion: ./appliusersion/controllers/users.php */