<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Type extends REST_Controller {

	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('m_type','mt');
	}

	public function index_get()
	{
		$type = $this->mt->get_all();

		if($type) {
			$this->response($type, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No type were found'
			], 404); 
		}
	}

}

/* End of file type.php */
/* Location: ./application/controllers/api/type.php */