<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class News extends REST_Controller {

	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('m_news','mn');
	}

	public function news_get() {
		$id = $this->get('id_news');

		if(!$id) {
			$this->response(NULL,400);
		}

		$cat = $this->mn->get_id($id);

		if($cat) {
			$this->response($cat, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No news were found'
			], 404); 
		}

	}

	public function user_get() {
		$id = $this->get('id_user');

		if(!$id) {
			$this->response(NULL,400);
		}

		$cat = $this->mn->get_all_peruser($id);

		if($cat) {
			$this->response($cat, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No news were found'
			], 404); 
		}

	}

	public function index_get() {
		$news = $this->mn->get_all();

		if($news) {
			$this->response($news, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No news were found'
			], 404); 
		}
	}

	public function main_get() {
		$news = $this->mn->get_main();

		if($news) {
			$this->response($news, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No news were found'
			], 404); 
		}
	}

	public function index_post() {

		$data = array(
		    'id_news' => $this->post('id_news'),
		    'id_categories' => $this->post('id_categories'),
		    'id_type' => $this->post('id_type'),
		    'id_user' => $this->post('id_user'), 
		    'title' => $this->post('title'),
		    'content' => $this->post('content'),
		    'active' => 'yes',
		    'created_at' => date('Y-m-d H:i:s'),
		    'modified_at' => null,
		    'deleted_at' => null
		);

		$res = $this->mn->post_data($data);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success, data inserted',
				'data' => $data
				],200);
		}

	}

	public function index_put() {

		$id = $this->put('id_news');
		$data = array(
			'id_news' => $this->put('id_news'),
			'id_categories' => $this->put('id_categories'),
			'id_type' => $this->put('id_type'), 
			'title' => $this->put('title'),
			'content' => $this->put('content'),
			'modified_at' => date('Y-m-d H:i:s'),
			'deleted_at' => null
		);

		$res = $this->mn->put_data($data,$id);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success, data updated',
				'data' => $data
				],200);
		}


	}

	public function del_put() {

		$id = $this->put('id_news');
		$data = array(
			'id_news' => $this->put('id_news'),
			'deleted_at' => date('Y-m-d H:i:s')
		);

		$res = $this->mn->softdel_data($data,$id);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success. data deleted',
				'data' => $data
				],200);
		}

	}

	public function deactive_put() {

		$id = $this->put('id_news');
		$data = array(
			'id_news' => $this->put('id_news'),
			'active' => 'no'
		);

		$res = $this->mn->deactive_data($data,$id);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success. a news has been deactivated',
				'data' => $data
				],200);
		}

	}

	public function active_put() {

		$id = $this->put('id_news');
		$data = array(
			'id_news' => $this->put('id_news'),
			'active' => 'yes'
		);

		$res = $this->mn->active_data($data,$id);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success. a news has been activated',
				'data' => $data
				],200);
		}

	}

}

/* End of file news.php */
/* Location: ./application/controllers/news.php */