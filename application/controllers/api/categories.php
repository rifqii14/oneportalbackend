<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class categories extends REST_Controller {

	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('m_categories','mc');
	}

	public function category_get() {
		$id = $this->get('id_categories');

		if(!$id) {
			$this->response(NULL,400);
		}

		$cat = $this->mc->get_id($id);

		if($cat) {
			$this->response($cat, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No categories were found'
			], 404); 
		}

	}

	public function index_get() {
		$categories = $this->mc->get_all();

		if($categories) {
			$this->response($categories, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No categories were found'
			], 404); 
		}
	}

	public function index_post() {

		$data = array(
		    'id_categories' => $this->post('id_categories'),
		    'categories' => $this->post('categories'),
		    'created_at' => date('Y-m-d H:i:s'),
		    'modified_at' => null
		);

		$res = $this->mc->post_data($data);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success, data inserted',
				'data' => $data
				],200);
		}

	}

	public function index_put() {

		$id = $this->put('id_categories');
		$data = array(
		    'id_categories' => $this->put('id_categories'),
		    'categories' => $this->put('categories'),
		    'modified_at' => date('Y-m-d H:i:s')
		);

		$res = $this->mc->put_data($data,$id);

		if($res === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success, data updated',
				'data' => $data
				],200);
		}


	}

	public function index_delete() {

		$id = $this->delete('id_categories');

		$del = $this->mc->delete_data($id);

		if($del === FALSE) {
			$this->response([
				'status' => 'failed'
				],502);
		}
		else {
			$this->response([
				'status' => 'success, data deleted',
				'id_categories' => $id
				],200);
		}

	}

}

/* End of file c_categories.php */
/* Location: ./application/controllers/c_categories.php */